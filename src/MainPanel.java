import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by f1410114 on 14/07/04.
 */
public class MainPanel extends JPanel implements MouseListener, MouseMotionListener, KeyListener{

    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    private Rect rect;
    private Circle circleRed, circleBlue, circleGreen;

    private boolean shiftPressed;
    private boolean ctrlPressed;

    public MainPanel () {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setFocusable(true);

        rect = new Rect(Color.white, 300, 200, 600, 400);
        circleRed = new Circle(Color.red, 150, 100, 50);
        circleBlue = new Circle(Color.blue, 50, 100, 50);
        circleGreen = new Circle(Color.green, 250, 100, 50);

        shiftPressed = false;
        ctrlPressed = false;

        addKeyListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void paintComponent (Graphics g) {
        rect.draw(g);
        circleBlue.draw(g);
        circleGreen.draw(g);
        circleRed.draw(g);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {}

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        int x = mouseEvent.getX();
        int y = mouseEvent.getY();
        circleRed.isHit(x, y);
        circleBlue.isHit(x, y);
        circleGreen.isHit(x, y);
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {}

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {}

    @Override
    public void mouseExited(MouseEvent mouseEvent) {}

    @Override
    public void mouseDragged(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        if (circleRed.hit && !ctrlPressed && !shiftPressed) {
            circleRed.moveTo(x, y);
            repaint();
        }
        if (circleBlue.hit && ctrlPressed) {
            circleBlue.moveTo(x, y);
            repaint();
        }
        if (circleGreen.hit && shiftPressed) {
            circleGreen.moveTo(x ,y);
            repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
            ctrlPressed = true;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = true;
        }

        if (keyEvent.getKeyCode() == KeyEvent.VK_PERIOD) {
            if (ctrlPressed) {
                circleBlue.becomeBigger();
            } else if (shiftPressed) {
                circleGreen.becomeBigger();
            } else {
                circleRed.becomeBigger();
            }
            repaint();
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_COMMA) {
            if (ctrlPressed) {
                circleBlue.becomeSmaller();
            } else if (shiftPressed) {
                circleGreen.becomeSmaller();
            } else {
                circleRed.becomeSmaller();
            }
            repaint();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_CONTROL) {
            ctrlPressed = false;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = false;
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {}
}
