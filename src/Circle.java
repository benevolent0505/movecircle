import java.awt.*;

/**
 * Created by f1410114 on 14/07/04.
 */
public class Circle {
    private Color color;
    private int xPos, yPos, radius;
    protected boolean hit;

    public Circle (Color color, int x, int y, int radius) {
        this.color = color;
        this.xPos = x;
        this.yPos = y;
        this.radius = radius;

        hit = false;
    }

    public void draw(Graphics g) {
        g.setColor(color);
        g.fillOval(xPos - radius, yPos - radius, radius*2,radius*2);
    }

    public void moveTo (int x, int y) {
        xPos = x;
        yPos = y;
    }

    public void isHit (int x, int y) {
        if (radius >= dist2pt(x,y,xPos,yPos)) {
            hit = true;
        } else {
            hit = false;
        }
    }

    private int dist2pt(int x, int y, int xPos, int yPos) {
        double distance = Math.sqrt((x-xPos)*(x-xPos) + (y-yPos)*(y-yPos));
        return (int)distance;
    }

    public void becomeBigger() {
        radius += 5;
    }

    public void becomeSmaller() {
        radius -= 5;
    }
}
