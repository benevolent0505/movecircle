import javax.swing.*;
import java.awt.*;

/**
 * Created by f1410114 on 14/07/04.
 */
public class MoveCircle extends JFrame{

    public MoveCircle () {
        setTitle("Move Circle");

        MainPanel panel = new MainPanel();
        Container contentPane = getContentPane();
        contentPane.add(panel);

        pack();
    }

    public static void main(String[] args) {
        MoveCircle frame = new MoveCircle();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
