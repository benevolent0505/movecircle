import java.awt.*;

/**
 * Created by f1410114 on 14/07/04.
 */
public class Rect {
    private Color color;
    private int xPos, yPos, width, height;

    public Rect (Color color, int x, int y, int width, int height) {
        this.color = color;
        this.xPos = x;
        this.yPos = y;
        this.width = width;
        this.height = height;
    }

    public void draw(Graphics g) {
        g.setColor(color);
        g.fillRect(xPos - width/2, yPos - height/2, width, height);
    }
}
